FROM nginx:1.15.1

RUN apt-get update && apt-get install -y \
    bzip2 git libcairo2-dev libjpeg-dev libgif-dev curl \
    gcc g++ make gnupg \
    && rm -rf /var/lib/apt/lists/*

RUN cd ~ && curl -sL https://deb.nodesource.com/setup_16.x | bash
RUN apt-get install -y nodejs

WORKDIR /app

COPY . /app
RUN npm install --save --legacy-peer-deps
RUN npm install caniuse-lite --save --legacy-peer-deps
RUN npm run build

RUN cp -R build/. /usr/share/nginx/html/admin
