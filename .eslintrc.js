module.exports = {
  root: true,
  parser: "babel-eslint",
  parserOptions: {
    sourceType: "module"
  },
  env: {
    browser: true,
    node: true,
    jquery: true,
    es6: true,
    jest: true
  },
  // extending recommended config and config derived from eslint-config-prettier
  extends: ["eslint:recommended", "prettier", "plugin:react/recommended"],
  // required to lint *.vue files
  plugins: ["html", "prettier",  "react", "eslint-plugin-import-order-alphabetical"],
  // add your custom rules here
  rules: {
    "prettier/prettier": "error",
    // allow paren-less arrow functions
    "arrow-parens": 0,
    // allow async-await
    "generator-star-spacing": 0,
    // allow debugger during development
    "no-debugger": process.env.NODE_ENV === "production" ? 2 : 0,
    // allow the use of console (no-console) only for warning and error messages
    "no-console": ["error", { allow: ["warn", "error"] }],
    "react/prop-types": 0,
    "max-len": ["error", { code: 100 }],
    "import-order-alphabetical/order": "error",
  },
  settings: {
    react: {
      version: "detect"
    }
  }
};
