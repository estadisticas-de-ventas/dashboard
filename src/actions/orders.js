import * as axios from 'axios';
import config from '../config';

export const getCampaignTotals = async (campaignNumber, search = null) => {
  const token = localStorage.getItem('token');
  const { data } = await axios.get(
    `${config.appServerUrls.ordersTotals}/${campaignNumber}${search}`,
    {
      headers: { Authorization: `Bearer ${token}` }
    }
  );
  return data;
};
