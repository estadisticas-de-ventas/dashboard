let config = {};

/* General config */

// AppServer URL
config.serverURL =
  process.env.REACT_APP_UC_APPSERVER_URL ||
  `${window.location.protocol}//${window.location.hostname}${
    window.location.port ? ':' + window.location.port : ''
  }/api`;

// AppServer endpoints
config.appServerUrls = {
  login: config.serverURL + '/auth/login',
  account: config.serverURL + '/auth/account',
  logout: config.serverURL + '/auth/logout',
  password: config.serverURL + '/auth/password',
  permissions: config.serverURL + '/auth/permissions',
  orders: config.serverURL + '/orders',
  ordersTotals: config.serverURL + '/orders/totals'
};

config.bingPictureApiUrl =
  'https://bing.biturl.top/?resolution=1920&format=image&index=0&mkt=zh-CN';

export default config;
