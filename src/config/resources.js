export const menuBadgeStatus = {
  new: ['records'],
  beta: ['users', 'regions', 'organizations'],
  legacy: ['groups']
};
