import * as React from 'react';
import { Route } from 'react-router-dom';
import Configuration from './components/configuration/configuration';

export default [<Route exact path="/configuration" component={Configuration} />];
