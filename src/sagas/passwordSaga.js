import { showNotification } from 'react-admin';
import { put, takeEvery } from 'redux-saga/effects';

export default function* passwordSaga() {
  yield takeEvery('PASSWORD_CHANGE_SUCCESS', function* () {
    yield put(showNotification('Constraseña actualizada'));
  });
  yield takeEvery('PASSWORD_CHANGE_FAILED', function* () {
    yield put(showNotification('Error al actualizar la contraseña', 'error'));
  });
}
