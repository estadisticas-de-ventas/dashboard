import moment from 'moment';
import simpleRestProvider from 'ra-data-simple-rest';
import { fetchUtils } from 'react-admin';
import utils, { deparam, exists, parseUrl, updateQueryStringParameter } from '../common';
import config from '../config';
import { checkUserState } from './authProvider';
import { checkUserLogin } from './authProvider';

const httpClient = (url, options = {}) => {
      checkUserState(); // Vencimiento de sesión por estado de usuario
      //checkUserLogin(); // Vencimiento de sesión mediante token
    
  if (utils.someEmptyUndefinedOrNull(options.headers)) {
    options.headers = new Headers({ Accept: 'application/json' });
  }
  const token = localStorage.getItem('token');
  options.headers.set('Authorization', `Bearer ${token}`);

  // Custom requests
  const parsedUrl = parseUrl(url);

  const { pathname: path, host, protocol } = parsedUrl;

  //console.log('parsedUrl', parsedUrl);
  //console.log('host', host);

  if (['/orders/totals', '/api/orders/totals'].includes(path)) {
    let { filter } = deparam(parsedUrl.search);
    filter = JSON.parse(filter);
    if (exists(filter.campaignNumber)) {
      // TODO: consider HTTPS
      const { campaignNumber } = filter;
      delete filter.campaignNumber;
      const locationSearch = updateQueryStringParameter(
        parsedUrl.search,
        'filter',
        encodeURIComponent(JSON.stringify(filter))
      );
      url = `${protocol}//${host}${path}/${campaignNumber}${locationSearch}`;
    }
  }
  //console.log('url', url);

  return fetchUtils.fetchJson(url, options);
};

export const DataProvider = simpleRestProvider(config.serverURL, httpClient);

/**
 * Format request filter dates
 * @param {Object} filters
 * @return
 */
const formatFilterDates = (filters) =>
  Object.keys(filters).forEach((k) =>
    moment.isMoment(filters[k]) ? (filters[k] = filters[k]['_d']) : null
  );

/**
 * Format request filter sort
 * @param {Object} sort
 * @return
 */
const formatSortField = (sort) =>
  exists(sort) && exists(sort.field) ? (sort.field = sort.field.replace('.value', '')) : null;

export default {
  getList: async (resource, params) => {
    formatFilterDates(params.filter);
    formatSortField(params.sort);
    return DataProvider.getList(resource, params);
  },
  getOne: async (resource, params) => {
    if (resource === 'orders/totals' && exists(params) && exists(params.search)) {
      formatFilterDates(params.search.filter);
      formatSortField(params.search.sort);
    }
    return DataProvider.getOne(resource, params);
  },
  getMany: async (resource, params) => DataProvider.getMany(resource, params),
  getManyReference: async (resource, params) => DataProvider.getManyReference(resource, params),
  update: async (resource, params) => DataProvider.update(resource, params),
  updateMany: async (resource, params) => DataProvider.updateMany(resource, params),
  create: async (resource, params) => DataProvider.create(resource, params),
  delete: async (resource, params) => DataProvider.delete(resource, params),
  deleteMany: async (resource, params) => DataProvider.deleteMany(resource, params)
};
