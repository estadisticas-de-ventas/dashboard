import axios from 'axios';
import { doesNotExist } from '../common';
import config from '../config';
import { jwtDecode } from "jwt-decode";
import { getAccessTokenApi } from "../auth/auth";

/**
 * Requests user login token.
 *
 * @param username                  - User's username.
 * @param password                  - Users's password.
 * @return {Promise<any>}       - Resolves promise login data or fails with error message
 */
const userLogin = async (username, password) => {
  try {
    return await axios.post(config.appServerUrls.login, {
      username,
      password
    });
  } catch (error) {
    throw new Error('Error de autenticación');
  }
};

export const checkUserLogin = () => {
  const accessToken = getAccessTokenApi();
  if (!accessToken) {
      userLogout();
  }
}


/**
 * Requests user login token.
 *
 * @return {Promise<any>}       - Resolves promise login data or fails with error message
 */
const getPermissions = async () => {
  const token = localStorage.getItem('token');
  try {
    const { data } = await axios.get(config.appServerUrls.permissions, {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-type': 'Application/json',
        Authorization: `Bearer ${token}`
      }
    });
    return data;
  } catch (error) {
    throw new Error('Permissions failed');
  }
};


// Función que realiza la solicitud HTTP para obtener el estado del usuario y maneja la respuesta
export function checkUserState() {
  const token = localStorage.getItem('token');

  if (!token) {
    console.log('Token no encontrado.');
    return;
  }

  const metaToken = jwtDecode(token);
  const { resellerNumber } = metaToken.user;

  axios.post(config.appServerUrls.account, { cuenta: resellerNumber }, {
    headers: {
      'Content-Type': 'application/json'
    },
  })
    .then((response) => {
      if (response.data.estado !== '04') {
        return true;
      } else {
        removeLoginData();
      }
    })
    .catch((err) => {
      removeLoginData();
      console.error(err);
    });
}

/**
 * Requests user login token.
 *
 * @return {Promise<any>}       - Resolves promise with logout data or fails with error message
 */
const userLogout = async () => {
  if (doesNotExist(localStorage.getItem('token'))) return;
  return Promise.resolve();
};

/**
 * Stores data from login request on localStorage.
 *
 * @param res         - response from login request
 */
function storeLoginData(res) {
  localStorage.setItem('token', res.token);
  localStorage.setItem('user', res.user);
  localStorage.setItem('role', res.role);
  localStorage.setItem('timestamp', new Date().getTime());
}

/**
 * Removes login localStorage data.
 */
export function removeLoginData() {
  localStorage.removeItem('token');
  localStorage.removeItem('user');
  localStorage.removeItem('timestamp');
  return Promise.resolve();
}

const authProvider = {
  login: (params) => {
    const { username, password } = params;
    return userLogin(username, password).then((response) => {
      if (![200, 204].includes(response.status)) {
        throw new Error(response.statusText);
      }
      storeLoginData(response.data);
      return response.data;
    });
  },
  logout: () => userLogout().finally(removeLoginData),
  checkAuth: () =>
    localStorage.getItem('user') === null || localStorage.getItem('token') === null
      ? Promise.reject()
      : Promise.resolve(),
  checkError: (error) => {
    const { status } = error;
    if ([401, 403].includes(status)) {
      try {
        removeLoginData();
      } finally {
        // eslint-disable-next-line no-unsafe-finally
        return Promise.reject();
      }
    }
    return Promise.resolve();
  },
  getPermissions: async () => getPermissions()
};

export default authProvider;
