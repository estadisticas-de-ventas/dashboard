import { jwtDecode } from "jwt-decode";

export function getAccessTokenApi() {
  const accessToken = localStorage.getItem('token');
  if (!accessToken || accessToken === "null") {
    return null;
  }
  return willExpireToken(accessToken) ? null : accessToken;
}


function willExpireToken(token) {
  const seconds = 1800; // Tiempo de expiración de Token de sesión.
  const metaToken = jwtDecode(token);
  const { iat } = metaToken; // Obtiene iat (Fecha/hora de token emitido en el inicio de sesión)

  const iatDate = new Date(iat * 1000); // Convertir iat a formato Date
  const expirationTime = iat + seconds; // Calcula el tiempo de expiración sumando seconds a iat
  const expirationTimeDate = new Date(expirationTime * 1000); // Convertir expirationTime a formato Date

  const now = Math.floor(Date.now() / 1000); // Tiempo actual en segundos desde Epoch

  console.log(`Tiempo de emisión (iat): ${iatDate.toLocaleString()}`);
  console.log(`Tiempo de expiración esperado: ${expirationTimeDate.toLocaleString()}`);

  if (now > expirationTime) {
     console.log('El token ha expirado.');
     localStorage.removeItem('token');
  }
}


