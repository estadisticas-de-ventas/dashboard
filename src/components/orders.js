import { useState } from 'react';
import LinearProgress from '@material-ui/core/LinearProgress';
import jsonExport from 'jsonexport/dist';
import React from 'react';
import {
  Pagination,
  BooleanInput,
  ReferenceInput,
  List,
  TextField,
  TextInput,
  Filter,
  downloadCSV,
  SelectInput,
  NumberInput,
  Button
} from 'react-admin';
import { DateInput } from 'react-admin';
import { doesNotExist, exists, updateQueryStringParameter } from '../common';
import CampaignField from './generic/campaignField';
import FormDialog from './generic/GetCampaignTotalButton';
import SectionInput from './generic/SectionInput';
import SellerInformationField from './generic/sellerInformationField';
import InputAdornment from '@material-ui/core/InputAdornment';
import MyCustomizableDatagrid from './common/MyCustomizableDatagrid';
import { AiOutlineQuestionCircle } from 'react-icons/ai';
import SimbolInformation from './generic/SimbolInformation/SimbolInformation';
import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';

const exportToXLS = (orders) => {
  const ordersForExport = orders.map((order) => {
    const {
      id,
      zoneCode,
      sectionCode,
      resellerNumber,
      name,
      address,
      city,
      provinceId,
      province,
      dni,
      phone,
      phone2,
      dateOfBirth,
      campaignDebt,
      campaignYearDebt,
      campaignOrders
    } = order;
    return {
      id,
      codigo_zona: zoneCode,
      codigo_seccion: sectionCode,
      numero_de_revendedora: resellerNumber,
      nombre: name,
      direccion: address,
      ciudad: city,
      provincia: provinceId,
      pronvicia: province,
      dni: dni,
      telefono: phone,
      telefono2: phone2,
      fecha_de_nacimiento: dateOfBirth,
      deuda_camp: campaignDebt,
      dauda_camp_y: campaignYearDebt,
      campaign_orders: campaignOrders
    };
  });

  const worksheet = XLSX.utils.json_to_sheet(ordersForExport);
  const workbook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(workbook, worksheet, 'Orders');
  const xlsData = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
  const blob = new Blob([xlsData], { type: 'application/octet-stream' });
  saveAs(blob, 'orders.xlsx');
};

const exporter = (orders) => {
  exportToXLS(orders);
};

const OrderFilter = ({ permissions, handleInfoModal, ...props }) => {
  return (
    <>
      {props.context === 'button' && (
        <Button onClick={handleInfoModal}>
          <AiOutlineQuestionCircle />
        </Button>
      )}
      <Filter {...props}>
        {/*<NumberInput label="Nº revendedora" source="resellerNumber" />*/}
        <NumberInput label="Nro de Revendedora" source="resellerNumber" />
        <ReferenceInput
          label="Zona"
          source="zone_id"
          reference="zones"
          alwaysOn={true}
          perPage={9999}
          sort={{ field: 'title', order: 'ASC' }}
          disabled={exists(permissions) ? permissions.role.code === 'ZDPR' : false}
          allowEmpty={false}
        >
          <SelectInput optionText="code" />
        </ReferenceInput>
        <SectionInput label="Sección" source="section_id" alwaysOn={true} />

        <TextInput label="Nombre" source="name" alwaysOn={true} />
        {/*<TextInput label="Ciudad" source="city" />*/}
        <TextInput label="DNI" source="dni" />
        <TextInput label="Teléfono 1" source="phone" />
        <TextInput label="Teléfono 2" source="phone2" />
        <TextInput label="Dirección" source="address" />
        {/*<TextInput label="Provincia" source="province" />*/}
        {/*<NumberInput
          source="campaignDebtFrom"
          label="Deuda desde"
          min={1}
          step={100}
          InputProps={{
            startAdornment: <InputAdornment position="start">$</InputAdornment>
          }}
        />*/}
        {/*<NumberInput
          source="campaignDebtTo"
          label="Deuda hasta"
          min={100}
          step={100}
          InputProps={{
            startAdornment: <InputAdornment position="start">$</InputAdornment>
          }}
        />*/}
        <DateInput
          source="dateOfBirthFrom"
          label="Cumpleaños Desde"
          options={{
            format: 'MMM dd'
          }}
        />
        <DateInput
          source="dateOfBirthTo"
          label="Cumpleaños Hasta"
          options={{
            format: 'MMM dd'
          }}
        />
        <SelectInput
          label="Campaña"
          source="campaignSelector"
          choices={permissions.campaigns.sort((a, b) => b.campaignNumber - a.campaignNumber)}
          optionValue="campaignNumber"
          optionText="campaignNumber"
          emptyText="Desactivar"
          resettable
        />
        {/*<BooleanInput label="Desarmes por Campaña" source="hasDisarm" />*/}
        <BooleanInput label="Desarmes" source="hasDisarm" />
        {/*<BooleanInput label="PC por Campaña" source="hasPayment" />*/}
        <BooleanInput label="PC" source="hasPayment" />
        {/*<BooleanInput label="Deuda por campaña" source="currentDebt" />*/}
        <BooleanInput label="Deuda" source="currentDebt" />
        <BooleanInput label="Cajas en  Depósito" source="deposit" />
        {/*<BooleanInput label="Pedidos por Campaña" source="campaignOrders" />*/}
        <BooleanInput label="Pedidos" source="campaignOrders" />
      </Filter>
    </>
  );
};

/*const exporter = (orders) => {
  const ordersForExport = orders.map((order) => {
    const {
      id,
      zoneCode,
      sectionCode,
      resellerNumber,
      name,
      address,
      city,
      provinceId,
      province,
      dni,
      phone,
      phone2,
      dateOfBirth,
      campaignDebt,
      campaignYearDebt,
      campaignOrders
    } = order;
    return {
      id,
      codigo_zona: zoneCode,
      codigo_seccion: sectionCode,
      numero_de_revendedora: resellerNumber,
      nombre: name,
      direccion: address,
      ciudad: city,
      provincia: provinceId,
      pronvicia: province,
      dni: dni,
      telefono: phone,
      telefono2: phone2,
      fecha_de_nacimiento: dateOfBirth,
      deuda_camp: campaignDebt,
      dauda_camp_y: campaignYearDebt,
      campaign_orders: campaignOrders
    };
  });
  jsonExport(
    ordersForExport,
    {
      headers: [
        'id',
        'codigo_zona',
        'codigo_seccion',
        'numero_de_revendedora',
        'nombre',
        'direccion',
        'ciudad',
        'provincia',
        'pronvicia',
        'dni',
        'telefono',
        'telefono2',
        'fecha_de_nacimiento',
        'deuda_camp',
        'dauda_camp_y',
        'campaign_orders'
      ] // order fields in the export
    },
    (err, csv) => {
      downloadCSV(csv, 'orders');
    }
  );
};*/

const postRowStyle = (record, index) => ({
  backgroundColor: index % 2 === 1 ? 'rgb(234, 234, 234)' : 'white'
});

const getCampaignColumnHeader = (campaignNumber, search) => (
  <FormDialog
    key={`${campaignNumber}_dialog`}
    campaignNumber={campaignNumber}
    search={search}
    test-id="gato"
  />
);

const PostPagination = (props) => (
  <Pagination rowsPerPageOptions={[10, 25, 50, 100, 500, 1000]} {...props} />
);

export const OrderList = ({ permissions, ...props }) => {
  const [openInfoModal, setOpenInfoModal] = useState(false);

  if (doesNotExist(permissions)) {
    return <LinearProgress />;
  }
  let locationSearch = props.location.search;

  const urlParams = new URLSearchParams(locationSearch);
  const urlDisplayedFilters = urlParams.get('displayedFilters');
  const urlParamFilter = urlParams.get('filter');
  const parsedFilter = JSON.parse(urlParamFilter);
  const parsedFilterActive = JSON.parse(urlDisplayedFilters);

  const urlParamZoneId = exists(parsedFilter) ? parsedFilter.zone_id : null;

  const zoneId =
    exists(permissions.user) && exists(permissions.user.zone) ? permissions.user.zone.id : 3;

  const urlParamCampaignSelector = exists(parsedFilter) ? parsedFilter.campaignSelector : null;

  const urlParamHasDisarm = exists(parsedFilter) ? parsedFilter.hasDisarm : null;
  const urlParamHasPayment = exists(parsedFilter) ? parsedFilter.hasPayment : null;
  const urlParamCampaignOrders = exists(parsedFilter) ? parsedFilter.campaignOrders : null;
  const urlParamdeposit = exists(parsedFilter) ? parsedFilter.deposit : null;
  const urlCurrentDebt = exists(parsedFilter) ? parsedFilter.currentDebt : null;

  const urlFilterParamHasDisarm = exists(parsedFilterActive) ? parsedFilterActive.hasDisarm : null;
  const urlFilterParamHasPayment = exists(parsedFilterActive) ? parsedFilterActive.hasPayment : null;
  const urlFilterParamCampaignOrders = exists(parsedFilterActive) ? parsedFilterActive.campaignOrders : null;
  const urlFilterParamdeposit = exists(parsedFilterActive) ? parsedFilterActive.deposit : null;
  const urlFilterParamDebt = exists(parsedFilterActive) ? parsedFilterActive.currentDebt : null;

  let campaigns = permissions.campaigns;
  let campaignsFilter = [];

  if (doesNotExist(urlParamZoneId)) {
    locationSearch = updateQueryStringParameter(
      locationSearch,
      'filter',
      `%7B%22zone_id%22%3A${zoneId}%7D`
    );
  }

  if (urlFilterParamCampaignOrders && typeof urlParamCampaignOrders == 'undefined') {
    const urlArrayO = locationSearch.split('&');
    let filterStrO = urlArrayO[1];
    filterStrO = `${filterStrO.replace('7D', `2C"campaignOrders"%3Afalse%7D`)}`;
    urlArrayO[1] = filterStrO;
    locationSearch = urlArrayO.join('&');
  }

  if (
    (urlCurrentDebt ||
      urlFilterParamHasDisarm ||
      urlFilterParamHasPayment ||
      urlFilterParamCampaignOrders ||
      urlFilterParamdeposit ||
      urlFilterParamDebt ||
      urlParamHasDisarm ||
      urlParamHasPayment ||
      urlParamCampaignOrders ||
      urlParamdeposit) &&
    (!urlParamCampaignSelector || typeof urlParamCampaignSelector == 'undefined')
  ) {
    
    const urlArray = locationSearch.split('&');
    let displayedFiltersStr = urlArray[0];

    urlArray[0] = displayedFiltersStr.replace('7D', '2C%22campaignSelector"%3Atrue%7D');

    if (
      !urlCurrentDebt &&
      (!urlParamHasDisarm || !urlParamHasPayment || !urlParamCampaignOrders || !urlParamdeposit)
    ) {
      const { campaignNumber } = campaigns[campaigns.length - 1];

      let filterStr = urlArray[1];

      filterStr = `${filterStr.replace('7D', `2C"campaignSelector"%3A"${campaignNumber}"%7D`)}`;

      urlArray[1] = filterStr;
    }

    locationSearch = urlArray.join('&');
  }

  // TODO: fix order

  props.location.search = locationSearch;
  campaigns = campaigns.sort((a, b) => a.campaignNumber - b.campaignNumber);

  let defaultCampaigns = campaigns;

  if (campaigns.length > 5) {
    defaultCampaigns = campaigns.slice(-5);
  }

  // campaigns = campaigns.sort((a, b) => b.campaignNumber - a.campaignNumber);

  defaultCampaigns = defaultCampaigns.map(({ campaignNumber }) => `records.${campaignNumber}`);

  const handleInfoModal = () => {
    setOpenInfoModal(!openInfoModal);
  };

  return (
    <List
      {...props}
      filters={
        <OrderFilter permissions={permissions} handleInfoModal={handleInfoModal} {...props} />
      }
      filterDefaultValues={{
        zone_id: zoneId
      }}
      bulkActionButtons={false}
      pagination={<PostPagination />}
      exporter={exporter}
      title="Estadística"
      sort={{ field: 'resellerNumber', order: 'DESC' }}
    >
      <>
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-end' }}>
          <SimbolInformation openInfoModal={openInfoModal} handleInfoModal={handleInfoModal} />
        </div>
        <MyCustomizableDatagrid
          defaultColumns={['resellerNumber', 'name', 'section', 'zone', ...defaultCampaigns]}
          style={{ marginTop: '30px' }}
          rowStyle={postRowStyle}
        >
          <SellerInformationField sortBy="resellerNumber" label="Número de cuenta" />

          <TextField source="zoneCode" label="Zona..." />
          <TextField source="sectionCode" label="Sección...." />
          {campaigns.map(({ campaignNumber }) => {
            return (
              <CampaignField
                key={campaignNumber}
                source={`records.${campaignNumber}`}
                sortable={false}
                label={getCampaignColumnHeader(campaignNumber, locationSearch)}
              />
            );
          })}
        </MyCustomizableDatagrid>
      </>
    </List>
  );
};
