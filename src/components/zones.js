import React from 'react';
import {
  ArrayField,
  List,
  TextField,
  TextInput,
  Filter,
  Datagrid,
  SingleFieldList,
  ChipField
} from 'react-admin';

const ZoneFilter = (props) => {
  return (
    <Filter {...props}>
      <TextInput label="ID" source="id" />
      <TextInput source="code" label="Codigo" />
      <TextInput source="title" label="Título" />
    </Filter>
  );
};

export const ZoneList = (props) => {
  return (
    <List {...props} filters={<ZoneFilter {...props} />} bulkActionButtons={false} perPage={25}>
      <Datagrid style={{ marginTop: '30px' }}>
        <TextField source="id" label="ID" />
        <TextField source="code" label="Codigo" />
        <TextField source="title" label="Título" />
        <ArrayField label="Sections" source="Sections">
          <SingleFieldList>
            <ChipField source="code" />
          </SingleFieldList>
        </ArrayField>
      </Datagrid>
    </List>
  );
};
