import React from 'react';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import variants from './variants.json';

const SIZE = 40;

export const Simbol = ({ variant }) => {
  return (
    <>
      {variants[variant] ? (
        <Box
          style={{
            backgroundColor: variants[variant].color,
            borderRadius: SIZE / 2,
            border: '1px solid #000000',
            width: SIZE,
            height: SIZE,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <Typography align="center" variant="h3" style={{ fontWeight: 700 }}>
            {variants[variant].text}
          </Typography>
        </Box>
      ) : null}
    </>
  );
};
