import Backdrop from '@material-ui/core/Backdrop';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Tooltip from '@material-ui/core/Tooltip';
import FunctionsIcon from '@material-ui/icons/Functions';
import React from 'react';
import { useMutation } from 'react-admin';
import { doesNotExist, exists } from '../../common';

const mainDivStyle = {
  textAlign: 'center',
  border: '1.5px solid rgb(33, 33, 33)',
  minWidth: '180px',
  minHeight: '170px',
  maxWidth: '250px',
  fontSize: '21px'
};

const useStyles = makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff'
  }
}));

export default function FormDialog({ campaignNumber, search }) {
  const [open, setOpen] = React.useState(false);
  const classes = useStyles();

  const searchParams = new URLSearchParams(search);
  const filterParam = searchParams.get('filter');
  const filter = JSON.parse(filterParam);
  filter.campaignNumber = campaignNumber;

  const [getTotals, { data, loading, loaded }] = useMutation({
    type: 'getList',
    resource: 'orders/totals',
    payload: {
      pagination: { page: 1, perPage: 10 },
      sort: { field: 'nothing', order: 'ASC' },
      filter
    }
  });

  const handleClickOpen = () => {
    setOpen(true);
    getTotals();
  };

  const handleClose = () => {
    setOpen(false);
  };

  const getCampaignCard = (cosmetics, extraCosmetics, totalCosmetics) => (
    <div style={mainDivStyle}>
      <div
        className={'campaignRow'}
        style={{ borderBottom: '1px solid rgb(33, 33, 33)', whiteSpace: 'nowrap' }}
      >
        <Tooltip
          title="Cosméticos"
          aria-label="cosmeticos"
          placement="top"
          style={{ cursor: 'help' }}
        >
          <div style={{ display: 'inline-block', minWidth: '49%' }}>
            {parseInt(~~cosmetics).toLocaleString('es-ES')}
          </div>
        </Tooltip>
        <div
          style={{
            display: 'inline-block',
            minWidth: '49%',
            borderLeft: '1px solid rgb(33, 33, 33)'
          }}
        >
          <Tooltip
            title="Extra cosméticos"
            aria-label="extra cosmeticos"
            placement="top"
            style={{ cursor: 'help' }}
          >
            <span>{parseInt(~~extraCosmetics).toLocaleString('es-ES')}</span>
          </Tooltip>
        </div>
      </div>
      {getSecondRow(' ', '')}
      <div style={{ marginTop: 30 }} />
      <div className={'campaignRow'} style={{ marginTop: '5px' }}>
        <Tooltip
          title="Total unidades"
          aria-label="Total unidades"
          placement="bottom"
          style={{ cursor: 'help' }}
        >
          <div>{parseInt(~~totalCosmetics).toLocaleString('es-ES')}</div>
        </Tooltip>
      </div>
    </div>
  );

  const getSecondRow = (sum, payment) => {
    return (
      <div className={'campaignRow'}>
        {sum} {exists(payment) ? payment : ''}
      </div>
    );
  };

  const getComparisonTable = (previous = null, current = null) => {
    if (doesNotExist(current)) {
      return <CircularProgress />;
    }
    return (
      <TableContainer component={Paper} style={{ minWidth: 350 }}>
        <Table size="small" aria-label="Totales">
          <TableHead>
            <TableRow>
              <TableCell align="left" style={{ textAlign: 'left' }}>
                <strong>Total</strong>
              </TableCell>
              {/* <TableCell align="right" style={{ textAlign: 'right' }}>
                <strong>Anterior</strong>
              </TableCell> */}
              <TableCell align="right" style={{ textAlign: 'right' }}>
                <strong>Actual</strong>
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow key={0}>
              <TableCell component="th" scope="row">
                <Tooltip
                  title="Total de pedidos"
                  aria-label="Total de pedidos"
                  placement="right"
                  style={{ cursor: 'help' }}
                >
                  <span>Pedidos</span>
                </Tooltip>
              </TableCell>
              {/* <TableCell align="right">
                {exists(previous)
                  ? parseInt(previous.totalRows).toLocaleString('es-ES')
                  : 'no disponible'}
              </TableCell> */}
              <TableCell align="right">
                {parseInt(current.totalRows).toLocaleString('es-ES')}
              </TableCell>
            </TableRow>
            {/* 
            <TableRow key={1}>
              <TableCell component="th" scope="row">
                <Tooltip
                  title="Primer pedido realizado"
                  aria-label="primer pedido realizado"
                  placement="right"
                  style={{ cursor: 'help' }}
                >
                  <span>Primeros pedidos</span>
                </Tooltip>
              </TableCell>
              <TableCell align="right">
                {exists(previous)
                  ? parseInt(previous.firstOrders).toLocaleString('es-ES')
                  : 'no disponible'}
              </TableCell>
              <TableCell align="right">
                {parseInt(current.firstOrders).toLocaleString('es-ES')}
              </TableCell>
            </TableRow>
            <TableRow key={2}>
              <TableCell component="th" scope="row">
                <Tooltip
                  title="Máximo 6 campañas sin pedir, hasta la actual"
                  aria-label="Máximo 6 campañas sin pedir, hasta la actual"
                  placement="right"
                  style={{ cursor: 'help' }}
                >
                  <span>Vuelven</span>
                </Tooltip>
              </TableCell>
              <TableCell align="right">
                {exists(previous)
                  ? parseInt(previous.comebacks).toLocaleString('es-ES')
                  : 'no disponible'}
              </TableCell>
              <TableCell align="right">
                {parseInt(current.comebacks).toLocaleString('es-ES')}
              </TableCell>
            </TableRow>
            <TableRow key={3}>
              <TableCell component="th" scope="row">
                <Tooltip
                  title="No pasaron mas de 6 campañas para atras"
                  aria-label=""
                  placement="right"
                  style={{ cursor: 'help' }}
                >
                  <span>Reinicio/reinco</span>
                </Tooltip>
              </TableCell>
              <TableCell align="right">
                {exists(previous)
                  ? parseInt(previous.reincos).toLocaleString('es-ES')
                  : 'no disponible'}
              </TableCell>
              <TableCell align="right">
                {parseInt(current.reincos).toLocaleString('es-ES')}
              </TableCell>
            </TableRow>
            <TableRow key={4}>
              <TableCell component="th" scope="row">
                <Tooltip
                  title="Cantidad de cajas en depósito"
                  aria-label=""
                  placement="right"
                  style={{ cursor: 'help' }}
                >
                  <span>Cajas en depósito</span>
                </Tooltip>
              </TableCell>
              
              <TableCell align="right">
                {/ TODO: Replace dismantling with cajas_en_deposito *}
                {exists(previous)
                  ? parseInt(previous.payment).toLocaleString('es-ES')
                  : 'no disponible'}
              </TableCell>
              <TableCell align="right">
                {parseInt(current.payment).toLocaleString('es-ES')}
              </TableCell>
            </TableRow>
            <TableRow key={5}>
              <TableCell component="th" scope="row">
                <Tooltip
                  title="Cantidad de desarmes"
                  aria-label=""
                  placement="right"
                  style={{ cursor: 'help' }}
                >
                  <span>Desarmes</span>
                </Tooltip>
              </TableCell>
              <TableCell align="right">
                {exists(previous)
                  ? parseInt(previous.dismantling).toLocaleString('es-ES')
                  : 'no disponible'}
              </TableCell>
              <TableCell align="right">
                {parseInt(current.dismantling).toLocaleString('es-ES')}
              </TableCell>
            </TableRow>
                */}
            <TableRow key={6}>
              <TableCell component="th" scope="row">
                <Tooltip
                  title="Deuda de 1 campañas atras"
                  aria-label="deuda de 1 campañas atras"
                  placement="right"
                  style={{ cursor: 'help' }}
                >
                  <span>Exigibles (D N-1)</span>
                </Tooltip>
              </TableCell>
              <TableCell align="right">
                {/*TODO: cambiar a deuda n-1 valor backend `debtPast` */}
                {exists(previous)
                  ? parseInt(previous.debtPast).toLocaleString('es-ES')
                  : 'no disponible'}
              </TableCell>
              <TableCell align="right">
                {parseInt(current.debtPast).toLocaleString('es-ES')}
              </TableCell>
            </TableRow>
            <TableRow key={7}>
              <TableCell component="th" scope="row">
                <Tooltip
                  title="Deuda de 2 campañas atras"
                  aria-label="deuda de 2 campañas atras"
                  placement="right"
                  style={{ cursor: 'help' }}
                >
                  <span>D Legal N-2</span>
                </Tooltip>
              </TableCell>
              <TableCell align="right">
                {exists(previous)
                  ? parseInt(previous.debtPast2).toLocaleString('es-ES')
                  : 'no disponible'}
              </TableCell>
              <TableCell align="right">
                {parseInt(current.debtPast2).toLocaleString('es-ES')}
              </TableCell>
            </TableRow>
            <TableRow key={8}>
              <TableCell component="th" scope="row">
                <Tooltip
                  title="Deuda de 3 campañas atras"
                  aria-label="deuda de 3 campañas atras"
                  placement="right"
                  style={{ cursor: 'help' }}
                >
                  <span>D Legal N-3</span> 
                </Tooltip>
              </TableCell>
              <TableCell align="right">
                {exists(previous)
                  ? parseInt(previous.debtPast3).toLocaleString('es-ES')
                  : 'no disponible'}
              </TableCell>
              <TableCell align="right">
                {parseInt(current.debtPast3).toLocaleString('es-ES')}
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    );
  };

  const getDialogContent = () => {
    if (loading || doesNotExist(data)) {
      return (
        <Backdrop className={classes.backdrop} open={loading}>
          <CircularProgress color="inherit" />
        </Backdrop>
      );
    }
    // Array fix for React Admin
    const [{ current, previous }] = data;
    return (
      <>
        <DialogTitle id="form-dialog-title">
          Totales Campaña <strong>{campaignNumber}</strong>
        </DialogTitle>
        <DialogContent>
          <div>
            <Grid container spacing={1} style={{ margin: 'auto' }}>
              <Grid item xs={6}>
                {getComparisonTable(previous, current)}
              </Grid>
              <Grid item xs={2} style={{ textAlign: 'center', margin: 'auto' }}>
                <FunctionsIcon style={{ fontSize: 150 }} />
              </Grid>
              <Grid item xs={4} style={{ margin: 'auto' }}>
                {getCampaignCard(current.cosmetics, current.extraCosmetics, current.totalCosmetics)}
              </Grid>
            </Grid>
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cerrar
          </Button>
        </DialogActions>
      </>
    );
  };

  return (
    <div>
      <div onClick={handleClickOpen} style={{ textAlign: 'center', cursor: 'pointer' }}>
        {campaignNumber}
      </div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
        maxWidth="md"
        fullWidth={true}
      >
        {getDialogContent()}
      </Dialog>
    </div>
  );
}
