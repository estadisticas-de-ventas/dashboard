import React from 'react';
import { ListButton, RefreshButton, CardActions } from 'react-admin';

export const GenericShowActions = ({ basePath }) => (
  <CardActions>
    <ListButton basePath={basePath} />
    <RefreshButton />
  </CardActions>
);
