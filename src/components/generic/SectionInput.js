import React from 'react';
import { useInput, useGetOne, SelectInput, LinearProgress } from 'react-admin';
import { useFormState } from 'react-final-form';
import { doesNotExist, exists } from '../../common';

const SectionInput = (props) => {
  useInput(props);

  const { values } = useFormState();

  const { data, loading, error: requestError } = useGetOne('zones', values.zone_id);
  if (loading || doesNotExist(data) || exists(requestError)) {
    return <LinearProgress />;
  }


  return (
    <SelectInput
      label="Sección"
      source="section_id"
      choices={data.Sections.sort((a, b) => (a.code > b.code ? 1 : b.code > a.code ? -1 : 0))}
      optionText="title"
      allowEmpty
      emptyValue={null}
      emptyText={'Todas'}
    />
  );
};
export default SectionInput;
