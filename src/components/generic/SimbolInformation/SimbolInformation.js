import { Typography } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import React from 'react';
import symbolInfoReference from '../../../assets/symbol-info-reference.jpg';

const useStyles = makeStyles((theme) => ({
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  },
  dialogContent: {
    padding: 0
  },
  image: {
    width: '100%',
    height: '100%'
  }
}));

const SimbolInformation = ({ openInfoModal, handleInfoModal }) => {
  const classes = useStyles();

  return (
    <Dialog
      open={openInfoModal}
      onClose={handleInfoModal}
      maxWidth="xl"
      fullWidth
    >
      <DialogTitle>
        <Typography variant="h6">Simbología</Typography>
        <IconButton aria-label="close" className={classes.closeButton} onClick={handleInfoModal}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent className={classes.dialogContent}>
          <img src={symbolInfoReference} className={classes.image} alt="Simbología" />
      </DialogContent>
    </Dialog>
  );
};

export default SimbolInformation;
