import React from 'react';
import { AiOutlineArrowRight } from 'react-icons/ai';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';

export const SimbolBox = ({ text, children }) => {
  return (
    <Box style={{ display: 'flex', alignItems: 'center', marginBottom: 5 }}>
      {children}
      <AiOutlineArrowRight />
      <Typography style={{ fontWeight: 700 }}> {text}</Typography>
    </Box>
  );
};
