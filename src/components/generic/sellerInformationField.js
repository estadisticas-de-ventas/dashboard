import Grid from '@material-ui/core/Grid';
import PhoneIcon from '@material-ui/icons/Phone';
import CardGiftcardIcon from '@material-ui/icons/Redeem';
import moment from 'moment';
import PropTypes from 'prop-types';
import * as React from 'react';
import { exists, numberWithDots } from '../../common';
import 'moment/locale/es';

const SellerInformationField = ({ record = {}, filters = {} }) => {
  const {
    resellerNumber,
    name,
    address,
    city,
    province,
    dateOfBirth,
    phone,
    phone2,
    campaignDebt,
    records,
    campaignYearDebt
  } = record;

  const birthday = exists(dateOfBirth) ? moment(dateOfBirth).format('DD/MM') : null;
  moment.locale('es');

  // Función para renderizar la deuda
  const renderDebt = () => {
    if (exists(filters.campaignSelector) && exists(filters.currentDebt) && filters.currentDebt === true && typeof records[filters.campaignSelector] !== 'undefined') {
      return (
        <Grid item xs={4}>
          ${numberWithDots(records[filters.campaignSelector].currentDebt)}{' '}
          {exists(filters.campaignSelector) ? (
            <span style={{ fontSize: 10 }}>({filters.campaignSelector})</span>
          ) : null}
        </Grid>
      );
    } else {
      return (
        <Grid item xs={4}>
          ${numberWithDots(campaignDebt)}{' '}
          {exists(campaignYearDebt) ? (
            <span style={{ fontSize: 10 }}>({campaignYearDebt})</span>
          ) : null}
        </Grid>
      );
    }
  };

  return (
    <div style={{ textAlign: 'left', maxWidth: 350, margin: 0 }}>
      <div className={'seller_info-row'}>
        <strong>
          {parseInt(resellerNumber)} - <span style={{ marginLeft: '5px' }}>{name}</span>{' '}
        </strong>
      </div>
      <div>
        {address}{city ? `, ${city}` : ''}{province ? ` - ${province} ` : ''}
      </div>
      <div className={'seller_info-row'}>
        <Grid container spacing={0}>
          <Grid item xs={4} style={{ textAlign: 'left' }}>
            {exists(phone) && phone !== '0' ? (
              <span>
                <PhoneIcon style={{ fontSize: '16px', verticalAlign: 'text-top' }} />
                {phone}
              </span>
            ) : null}
          </Grid>
          <Grid item xs={4} style={{ textAlign: 'left' }}>
            {exists(phone2) && phone2 !== '0' ? (
              <span>
                <PhoneIcon style={{ fontSize: '16px', verticalAlign: 'text-top' }} />
                {phone2}
              </span>
            ) : null}
          </Grid>
          <Grid item xs={5}>
            {exists(dateOfBirth) ? (
              <span>
                <CardGiftcardIcon
                  style={{ fontSize: '16px', verticalAlign: 'text-top', marginRight: 3 }}
                />
                {moment(dateOfBirth).format('DD/MM/YYYY')}
              </span>
            ) : (
              ''
            )}
          </Grid>
        </Grid>
      </div>
      <div>
        <Grid container spacing={0}>
          <Grid item>Deuda:</Grid>
          {renderDebt()}
        </Grid>
      </div>
    </div>
  );
};

SellerInformationField.propTypes = {
  label: PropTypes.string,
  record: PropTypes.object,
  filters: PropTypes.object,
};

export default SellerInformationField;
