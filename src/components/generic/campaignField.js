import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import * as React from 'react';
import { doesNotExist, exists, numberWithDots } from '../../common';

const decompressValue = (source, record) => {
  let value = record;
  let undefinedFound = false;
  source
    .split('.')
    .forEach((w) => (exists(value[w]) ? (value = value[w]) : (undefinedFound = true)));
  return undefinedFound ? undefined : value;
};

const getFirstRow = (cosmetics, extraCosmetics, repayment) => (
  <div
    className={'campaignRow'}
    style={{ borderBottom: '1px solid rgb(33, 33, 33)', whiteSpace: 'nowrap' }}
  >
    <div style={{ display: 'inline-block', width: '38%' }}>{cosmetics}</div>
    <div style={{ display: 'inline-block', width: '56%', borderLeft: '1px solid rgb(33, 33, 33)' }}>
      {extraCosmetics}
      {exists(repayment) && <span> *</span>}
    </div>
  </div>
);

const getSecondRow = (sum, payment) => {
  return (
    <div className={'campaignRow'}>
      {sum} {exists(payment) ? payment : ''}
    </div>
  );
};

const getThirdRow = (dismantling, deposit, ticketsEcommerce, currentDebt, debtYear) => {
  const wrapContent = (value = '') => (
    <div className={'campaignRow'} style={{ minHeight: '40px', marginTop: '5px' }}>
      {value}
      {ticketsEcommerce > 0 && <span>{ticketsEcommerce}</span>}
    </div>
  );

  if (exists(dismantling)) return wrapContent('D');

  if (doesNotExist(currentDebt) || doesNotExist(debtYear)) return wrapContent();

  return (
    <div className={'campaignRow'} style={{ marginTop: '5px' }}>
      {exists(deposit) && <span>{deposit}</span>}
      {ticketsEcommerce > 0 && <span>{ticketsEcommerce}</span>}
      <div>${numberWithDots(currentDebt.toLocaleString('es-ES'))}</div>
    </div>
  );
};

const mainDivStyle = {
  textAlign: 'center',
  border: '1.5px solid rgb(33, 33, 33)',
  // minWidth: '70px',
  // minHeight: '90px',
  maxWidth: '80px',
  height: '80px'
};

const CampaignField = ({ source, record = {} }) => {
  const value = decompressValue(source, record);
  if (doesNotExist(value) || doesNotExist(value.cosmetics) || doesNotExist(value.extraCosmetics)) {
    return (
      <Grid container direction="row-reverse" justify="center" alignItems="center">
        <div
          className={'campaignRow'}
          style={{ borderBottom: '1px solid rgb(33, 33, 33)', whiteSpace: 'nowrap' }}
        >
          <div style={{ display: 'inline-block', width: '38%' }}> </div>
          <div
            style={{
              display: 'inline-block',
              width: '56%',
              borderLeft: '1px solid rgb(33, 33, 33)'
            }}
          >
            {' '}
          </div>
        </div>
        {getSecondRow(' ', '')}
        <div className={'campaignRow'} style={{ marginTop: '5px' }}>
          <div> </div>
          <div> </div>
        </div>
      </Grid>
    );
  }
  const {
    cosmetics,
    extraCosmetics,
    payment,
    dismantling,
    repayment,
    currentDebt,
    debtYear,
    debtCampaignNumber,
    preInvoice,
    deposit,
    ticketsEcommerce
  } = value;

  const painted = {};
  if (preInvoice === 'p' || preInvoice === 'P') {
    painted.backgroundColor = 'rgba(245, 249, 193, 0.68)';
  }

  const blue = {};
  if (deposit) {
    blue.backgroundColor = 'rgba(132, 205, 232, 0.68)';
  }

  return (
    <Grid container direction="row-reverse" justify="center" alignItems="center">
      <Grid item xs={12} style={{ ...mainDivStyle, ...painted, ...blue }}>
        {getFirstRow(
          parseInt(cosmetics).toLocaleString('es-ES'),
          parseInt(extraCosmetics).toLocaleString('es-ES'),
          repayment
        )}
        {getSecondRow(parseInt(cosmetics + extraCosmetics).toLocaleString('es-ES'), payment)}
        {getThirdRow(
          dismantling,
          deposit,
          ticketsEcommerce,
          parseInt(currentDebt).toLocaleString('es-ES'),
          `${debtYear}${debtCampaignNumber}`
        )}
      </Grid>
    </Grid>
  );
};
CampaignField.propTypes = {
  label: PropTypes.string,
  record: PropTypes.object,
  source: PropTypes.string.isRequired
};

export default CampaignField;
