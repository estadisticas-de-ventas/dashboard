export const genericStyles = {
  inlineBlock: { display: 'inline-flex', marginRight: '1rem' },
  fullWidth: { width: '100%' }
};
