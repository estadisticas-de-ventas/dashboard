import React from 'react';
import { List, TextField, TextInput, Filter, Datagrid } from 'react-admin';

const RegionFilter = (props) => {
  return (
    <Filter {...props}>
      <TextInput label="ID" source="id" />
      <TextInput source="code" label="Codigo" />
      <TextInput source="title" label="Título" />
    </Filter>
  );
};

export const RegionList = (props) => {
  return (
    <List {...props} filters={<RegionFilter {...props} />} bulkActionButtons={false} perPage={25}>
      <Datagrid style={{ marginTop: '30px' }}>
        <TextField source="id" label="ID" />
        <TextField source="code" label="Codigo" />
        <TextField source="title" label="Título" />
      </Datagrid>
    </List>
  );
};
