import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { makeStyles } from '@material-ui/core/styles';
import * as React from 'react';
import { Title } from 'react-admin';
import { useSelector, useDispatch } from 'react-redux';
import { changeTheme } from './actions';
import PasswordFormDialog from './PasswordDialog';

const useStyles = makeStyles({
  label: { width: '10em', display: 'inline-block' },
  button: { margin: '1em' }
});

const Configuration = () => {
  const classes = useStyles();
  const theme = useSelector((state) => state.theme);
  const dispatch = useDispatch();
  return (
    <Card style={{ marginTop: 5 }}>
      <Title title="Configuración" />
      <CardContent>
        <div className={classes.label}>Theme</div>
        <Button
          variant="contained"
          className={classes.button}
          color={theme === 'light' ? 'primary' : 'default'}
          onClick={() => dispatch(changeTheme('light'))}
        >
          Light
        </Button>
        <Button
          variant="contained"
          className={classes.button}
          color={theme === 'dark' ? 'primary' : 'default'}
          onClick={() => dispatch(changeTheme('dark'))}
        >
          Dark
        </Button>
        <PasswordFormDialog />
      </CardContent>
    </Card>
  );
};

export default Configuration;
