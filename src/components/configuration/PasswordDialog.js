import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { TextField } from 'mui-rff';
import React from 'react';
import { Form } from 'react-final-form';
import { useDispatch } from 'react-redux';
import { performPasswordChange } from './actions';

export default function PasswordFormDialog() {
  const [open, setOpen] = React.useState(false);
  const dispatch = useDispatch();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const onSubmit = async ({ newPassword }) => {
    await dispatch(performPasswordChange(newPassword));
  };

  async function validate(values) {
    if (values.newPassword !== values.newPasswordDuplicate) {
      return { newPasswordDuplicate: 'Las contraseñas deben coincidir.' };
    }
    return;
  }

  return (
    <>
      <div>
        <Button variant="outlined" color="primary" onClick={handleClickOpen}>
          Cambiar contraseña
        </Button>
      </div>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Cambiar contraseña</DialogTitle>
        <DialogContent>
          <Form
            validate={validate}
            onSubmit={onSubmit}
            initialValues={{ newPassword: '', newPasswordDuplicate: '' }}
            render={({ handleSubmit, form, submitting, pristine, values }) => (
              <form onSubmit={handleSubmit} noValidate>
                {' '}
                <div>
                  <TextField
                    autoFocus
                    margin="dense"
                    name="newPassword"
                    label="Nueva contraseña"
                    fullWidth
                  />
                  <TextField
                    margin="dense"
                    name="newPasswordDuplicate"
                    label="Confirmar contraseña"
                    fullWidth
                  />
                </div>
                <div style={{ textAlign: 'right' }}>
                  <Button onClick={handleClose} color="primary" type="button">
                    Cancelar
                  </Button>
                  <Button type="submit" color="primary" variant="contained" disabled={submitting}>
                    Confirmar
                  </Button>
                </div>
              </form>
            )}
          />
        </DialogContent>
      </Dialog>
    </>
  );
}
