import { put } from 'axios';
import { getErrorMessageFromRequest } from '../../common';
import config from '../../config';
import {removeLoginData} from "../../auth/authProvider";

export const CHANGE_THEME = 'CHANGE_THEME';
export const PASSWORD_CHANGE_BEGAN = 'PASSWORD_CHANGE_BEGAN';
export const PASSWORD_CHANGE_SUCCESS = 'PASSWORD_CHANGE_SUCCESS';
export const PASSWORD_CHANGE_FAILED = 'PASSWORD_CHANGE_FAILED';

export const changeTheme = (theme) => ({
  type: CHANGE_THEME,
  payload: theme
});

export const startPasswordChange = () => ({
  type: PASSWORD_CHANGE_BEGAN
});

export const passwordChangeSuccess = () => ({
  type: PASSWORD_CHANGE_SUCCESS
});

export const passwordChangeFailed = (message) => ({
  type: PASSWORD_CHANGE_FAILED,
  payload: message
});

export const performPasswordChange = (password) => async (dispatch) => {
  dispatch(startPasswordChange());
  const token = localStorage.getItem('token');
  try {
    await put(
      config.appServerUrls.password,
      { password: password },
      {
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-type': 'Application/json',
          Authorization: `Bearer ${token}`
        }
      }
    );
    dispatch(passwordChangeSuccess());
    removeLoginData();
    window.location = '/#/login';
    return;
  } catch (error) {
    return dispatch(passwordChangeFailed(getErrorMessageFromRequest(error)));
  }
};
