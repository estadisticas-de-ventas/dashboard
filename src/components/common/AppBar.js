import Avatar from '@material-ui/core/Avatar';
import CircularProgress from '@material-ui/core/CircularProgress';
import { deepOrange, deepPurple } from '@material-ui/core/colors';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import DashboardIcon from '@material-ui/icons/Dashboard';
import SettingsIcon from '@material-ui/icons/Settings';
import * as React from 'react';
import { forwardRef } from 'react';
import { AppBar, MenuItemLink, UserMenu, usePermissions } from 'react-admin';
import { doesNotExist, titleCase } from '../../common';

const useStyles = makeStyles({
  title: {
    flex: 1,
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap',
    overflow: 'hidden'
  },
  spacer: {
    flex: 1
  },
  avatar: {
    height: 30,
    width: 30,
    color: 'white',
    backgroundColor: deepOrange[500]
  },
  orange: {
    color: deepOrange[500],
    backgroundColor: deepOrange[500]
  },
  purple: {
    color: deepPurple[500],
    backgroundColor: deepPurple[500]
  },
  menuButton: {
    display: 'none' // Oculta el botón del menú
  }
});

const getCustomColorByCode = (roleCode) => {
  switch (roleCode) {
    case 'ZDGN':
      return '#e91e63';
    case 'ZDJV':
      return '#00bcd4';
    case 'ZDGE':
      return '#2196f3';
    case 'ZDPR':
      return '#009688';
    case 'ZDLI':
      return '#8bc34a';
    case 'ZDRE':
      return '#ffeb3b';
    case 'ZDAS':
      return '#ff9800';
    case 'ZDCO':
      return '#ff5722';
    default:
      return 'gray';
  }
};

const MyCustomIcon = () => {
  const classes = useStyles();
  const { permissions } = usePermissions();
  if (doesNotExist(permissions)) {
    return <CircularProgress />;
  }
  const matches = permissions.role.title.match(/\b(\w)/g);
  const roleAvatar = matches.join('').substring(0, 0);  // MM - que no salga el Rol en el Icon

  return (
    <>
      <span style={{ maxHeight: 30, fontSize: 17, marginRight: 5 }}>
        {titleCase(permissions.user.name.toLowerCase())} -
      </span>
      <span style={{ maxHeight: 30, fontSize: 20, marginRight: 5 }}>
        {permissions.user.resellerNumber}
      </span>
      <Avatar
        className={classes.avatar}
        style={{ backgroundColor: getCustomColorByCode(permissions.role.code) }}
      >
        {roleAvatar}
      </Avatar>
    </>
  );
};

const ConfigurationMenu = forwardRef(({ onClick }, ref) => (
  <MenuItemLink
    ref={ref}
    to="/configuration"
    primaryText="Configuración"
    leftIcon={<SettingsIcon />}
    onClick={onClick} // close the menu on click
  />
));

const DashboardMenu = forwardRef(({ onClick }, ref) => (
  <MenuItemLink
    ref={ref}
    to="/"
    primaryText="Estadísticas"
    leftIcon={<DashboardIcon />}
    onClick={onClick} // close the menu on click
  />
));

const MyUserMenu = (props) => (
  <UserMenu {...props} icon={<MyCustomIcon />}>
    <DashboardMenu />
    <ConfigurationMenu />
  </UserMenu>
);

const MyAppBar = (props) => {
  const classes = useStyles();
  return (
    <AppBar {...props} userMenu={<MyUserMenu />} classes={classes}>
      {/*<Typography variant="h6" color="inherit" className={classes.title} id="react-admin-title" />*/}
      <img src={process.env.PUBLIC_URL + '/violetta_bar.png'} style={{ maxHeight: 48, marginLeft: 20 }} alt="" />
      <span className={classes.spacer} />
    </AppBar>
  );
};

export default MyAppBar;
