import { withStyles } from '@material-ui/core/styles';
import React from 'react';
import { Login, LoginForm } from 'react-admin';

const styles = {
  main: {
    // backgroundImage: `url(${config.bingPictureApiUrl})`,
    backgroundImage: `url(https://lh3.googleusercontent.com/meCF3H3AsWTcs0xfPBXgrLeBlnU83MBWpn0vM2o_IlmnGZMgyPUNLLJXYY_pyEINBmY)`,
    height: '100%',
    backgroundPosition: 'center',
    backgroundRepeat: 'repeat',
    backgroundSize: '250px'
  }
};

const CustomLoginForm = withStyles({
  button: { background: '#F15922' }
})(LoginForm);

const CustomLoginPage = (props) => <Login loginForm={<CustomLoginForm />} {...props} />;

export default withStyles(styles)(CustomLoginPage);
