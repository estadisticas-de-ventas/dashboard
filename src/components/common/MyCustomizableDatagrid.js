import CustomizableDatagrid from 'ra-customizable-datagrid';
import React from 'react';

class MyCustomizableDatagrid extends CustomizableDatagrid {
  constructor(props) {
    super(props);
  }

  getColumnLabels() {
    const result = super.getColumnLabels();
    const finalResult = [];
    let campaigns = [];

    result.forEach((item) => {
      if (item.label instanceof Object) {
        campaigns.push(item);
      } else {
        finalResult.push(item);
      }
    });

    campaigns = campaigns.sort(
      (a, b) => b.label.props.campaignNumber - a.label.props.campaignNumber
    );

    finalResult.push(...campaigns);

    return finalResult;
  }
}

export default MyCustomizableDatagrid;
