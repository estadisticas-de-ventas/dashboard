import React from 'react';
import { Layout } from 'react-admin';
import { useSelector } from 'react-redux';
import { darkTheme, lightTheme } from '../configuration/themes';
import MyAppBar from './AppBar';
import Menu from './Menu';

const MyLayout = (props) => {
  const theme = useSelector((state) => state.theme);

  return (
    <Layout
      {...props}
      menu={Menu}
      appBar={MyAppBar}
      theme={theme === 'dark' ? darkTheme : lightTheme}
    />
  );
};

export default MyLayout;
