import { makeStyles } from '@material-ui/core/styles';
import React from 'react';
import {
  ArrayField,
  List,
  TextField,
  TextInput,
  Filter,
  Datagrid,
  SingleFieldList,
  ChipField
} from 'react-admin';

const SectionFilter = (props) => {
  return (
    <Filter {...props}>
      <TextInput label="ID" source="id" />
      <TextInput source="code" label="Codigo" />
      <TextInput source="title" label="Título" />
    </Filter>
  );
};

export const SectionList = (props) => {
  return (
    <List {...props} filters={<SectionFilter {...props} />} bulkActionButtons={false} perPage={25}>
      <Datagrid style={{ marginTop: '30px' }}>
        <TextField source="id" label="ID" />
        <TextField source="code" label="Codigo" />
        <TextField source="title" label="Título" />
        <ArrayField label="Zonas" source="Zones">
          <SingleFieldList>
            <ChipField source="code" />
          </SingleFieldList>
        </ArrayField>
      </Datagrid>
    </List>
  );
};
