export default (
  previousState = {
    submitting: false,
    success: false,
    error: false,
    errorMessage: ''
  },
  { type, payload }
) => {
  if (type === 'PASSWORD_CHANGE_BEGAN') {
    return Object.assign({}, previousState, {
      submitting: true,
      success: false,
      error: false,
      errorMessage: ''
    });
  }
  if (type === 'PASSWORD_CHANGE_SUCCESS') {
    return Object.assign({}, previousState, {
      submitting: false,
      success: true,
      error: false,
      errorMessage: ''
    });
  }
  if (type === 'PASSWORD_CHANGE_FAILED') {
    return Object.assign({}, previousState, {
      submitting: false,
      success: false,
      error: true,
      errorMessage: payload
    });
  }
  return previousState;
};
