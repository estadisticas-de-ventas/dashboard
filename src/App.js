import spanishMessages from '@blackbox-vision/ra-language-spanish';
import { createMuiTheme } from '@material-ui/core/styles';
import PolicyRoundedIcon from '@material-ui/icons/PolicyRounded';
import polyglotI18nProvider from 'ra-i18n-polyglot';
import React from 'react';
import './App.css';
import { Admin, Resource } from 'react-admin';
import { Provider } from 'react-redux';
import authProvider from './auth/authProvider';
import dataProvider from './auth/dataProvider';
import Layout from './components/common/Layout';
import LoginPage from './components/common/Login';
import { OrderList } from './components/orders';
import createAdminStore from './createAdminStore';
import passwordReducer from './reducers/passwordReducer';
import themeReducer from './reducers/themeReducer';
import routes from './routes';
import { createHashHistory } from 'history';

const theme = createMuiTheme({
  palette: {
    // type: 'dark',
    secondary: {
      // main: '#1976d2' blue
      main: '#4a2b6e'
    }
    // primary: {
    //   main: '#e1bee7'
    // }
  }
});

const i18nProvider = polyglotI18nProvider(() => spanishMessages, 'es');
const history = createHashHistory();

const App = () => (
  <Provider
    store={createAdminStore({
      authProvider,
      dataProvider,
      history
    })}
  >
    <Admin
      theme={theme}
      layout={Layout}
      loginPage={LoginPage}
      title="Violetta - Estadisticas"
      authProvider={authProvider}
      dataProvider={dataProvider}
      history={history}
      i18nProvider={i18nProvider}
      customReducers={{ theme: themeReducer, password: passwordReducer }}
      customRoutes={routes}
    >
      <Resource
        name="orders"
        icon={PolicyRoundedIcon}
        list={OrderList}
        options={{ label: 'Estadisticas' }}
      />
      <Resource name="zones" icon={PolicyRoundedIcon} options={{ label: 'Zonas' }} />
      <Resource name="sections" icon={PolicyRoundedIcon} options={{ label: 'Secciónes' }} />
      <Resource name="regions" icon={PolicyRoundedIcon} options={{ label: 'Regiones' }} />
      <Resource name="orders/totals" icon={PolicyRoundedIcon} options={{ label: 'Totals' }} />
    </Admin>
  </Provider>
);
export default App;
