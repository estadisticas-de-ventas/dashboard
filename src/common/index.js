/* eslint no-extra-parens: ["error", "functions"] */
import moment from 'moment';

/**
 * Check if a variable is undefined
 * @param  {[type]}  item - Item to check
 * @return {Boolean} true: if argument is undefined
 *                   false: if argument is defined
 */
export const isUndefined = (item) => {
  return typeof item === 'undefined';
};

/**
 * Check if a variable is defined
 * @param  {[type]}  item - Item to check
 * @return {Boolean} true: if argument is defined
 *                   false: if argument is undefined
 */
export const isDefined = (item) => {
  return !isUndefined(item);
};

/**
 * Check if at least one argument is undefined
 * @param {arguments[]} arguments   Arguments to check for
 * @return {Boolean} true: at least one argument is undefined
 *                   false: all arguments are defined
 */
export const someUndefined = (...args) => {
  for (let i = 0; i < args.length; i++) {
    if (isUndefined(args[i])) return true;
  }

  return false;
};

/**
 * Check if at least one argument is undefined, null or an empty object
 * @param {arguments[]} arguments   Arguments to check for
 * @return {Boolean} true: at least one argument is undefined, null or an empty object
 *                   false: all arguments are defined
 */
export const someEmptyUndefinedOrNull = (...args) => {
  for (let i = 0; i < args.length; i++) {
    if (isUndefined(args[i]) || args[i] === '' || args[i] === null) {
      return true;
    }

    if (typeof args[i] === 'object') {
      if (Array.isArray(args[i]) && args[i].length === 0) return true;
      if (Object.keys(args[i]).length === 0) return true;
    }
  }

  return false;
};

/**
 * Check if all arguments are undefined
 * @param {arguments[]} arguments   Arguments to check for
 * @return {Boolean} true: all arguments are undefined
 *                   false: at least one argument is defined.
 */
export const allUndefined = (...args) => {
  for (let i = 0; i < args.length; i++) {
    if (isUndefined(args[i])) return false;
  }
  return true;
};

/**
 *
 * @param {String} email      - Email
 * @return {Boolean}          - True if email is valid
 */
export const validateEmailFormat = (email) => {
  const re = /\S+@\S+\.\S+/;
  return re.test(email);
};

/**
 * Checks if param is undefined, empty or null
 *
 * @param {String|Object|Array} param       - Param to check
 * @return {boolean}                        - True if param fails any of this check. False otherwise
 */
export const doesNotExist = (param) => {
  if (typeof param === 'undefined' || param === '' || param === null) return true;
  if (Array.isArray(param) && param.length === 0) return true;
  if (typeof param === 'object' && Object.prototype.toString.call(param) !== '[object Date]') {
    return Object.keys(param).length === 0;
  }
  return false;
};

/**
 * Checks if param exists
 *
 * @param {String|Object|Array} param       - Param to check
 * @return {boolean}                        - True if param fails any of this check. False otherwise
 */
export const exists = (param) => !doesNotExist(param);

/**
 * Capitalize string
 *
 * @param {String} s        - String to be capitalize
 * @returns {*}             - Capitalized string
 */
export const capitalize = (s) => (exists(s) ? s.replace(/^./g, s[0].toUpperCase()) : '');

export const prefixExporterWatermark = (title) =>
  `urbansim-admin-${title}-export-${moment().format('YYYY_MM_DD-h_mm_ss')}`;

export const hasUnlimitedProjects = (organization) =>
  exists(organization.pencilerSubscription)
    ? organization.pencilerSubscription.subscriptionOption.isUnlimited
    : false;

export const numberWithDots = (x) =>
  exists(x) ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.') : null;

/**
 * Return error string based on Axios failed request response.
 *
 * @param {Object} error        - Axios failed response.
 * @return {string}             - Error message.
 */
export const getErrorMessageFromRequest = (error) => {
  if (error.response && error.response.status === 404) return 'API endpoint not found';
  if (error.response) return error.response.data.message;
  if (error.request) return 'Server not found';
  return 'Server error';
};

/**
 * Capitalize each word of string
 *
 * @param {String} str        - String input
 * @return {string}           - String output
 */
export const titleCase = (str) => {
  const splitStr = str.toLowerCase().split(' ');
  for (let i = 0; i < splitStr.length; i++) {
    splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }
  return splitStr.join(' ');
};

/**
 * Update query string parameter
 *
 * @param {String} uri
 * @param {String} key
 * @param {String} value
 * @return {string|void|boolean|CallHistoryMethodAction<[, string]>|CallHistoryMethodAction<[<>]>|*}
 */
export const updateQueryStringParameter = (uri, key, value) => {
  const re = new RegExp('([?&])' + key + '=.*?(&|$)', 'i');
  const separator = uri.indexOf('?') !== -1 ? '&' : '?';
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + '=' + value + '$2');
  }
  return uri + separator + key + '=' + value;
};

export const parseUrl = (url) => {
  let m = url.match(
      /^(([^:\/?#]+:)?(?:\/\/((?:([^\/?#:]*):([^\/?#:]*)@)?([^\/?#:]*)(?::([^\/?#:]*))?)))?([^?#]*)(\?[^#]*)?(#.*)?$/
    ),
    r = {
      hash: m[10] || '', // #asd
      host: m[3] || '', // localhost:257
      hostname: m[6] || '', // localhost
      href: m[0] || '', // http://username:password@localhost:257/deploy/?asd=asd#asd
      origin: m[1] || '', // http://username:password@localhost:257
      pathname: m[8] || (m[1] ? '/' : ''), // /deploy/
      port: m[7] || '', // 257
      protocol: m[2] || '', // http:
      search: m[9] || '', // ?asd=asd
      username: m[4] || '', // username
      password: m[5] || '' // password
    };
  if (r.protocol.length == 2) {
    r.protocol = 'file:///' + r.protocol.toUpperCase();
    r.origin = r.protocol + '//' + r.host;
  }
  r.href = r.origin + r.pathname + r.search + r.hash;
  return r;
};

export const deparam = (function (d, x, params, p, i, j) {
  return function (qs) {
    // start bucket; can't cheat by setting it in scope declaration or it overwrites
    params = {};
    // remove preceding non-querystring, correct spaces, and split
    qs = qs
      .substring(qs.indexOf('?') + 1)
      .replace(x, ' ')
      .split('&');
    // march and parse
    for (i = qs.length; i > 0; ) {
      p = qs[--i];
      // allow equals in value
      j = p.indexOf('=');
      // what if no val?
      if (j === -1) params[d(p)] = undefined;
      else params[d(p.substring(0, j))] = d(p.substring(j + 1));
    }

    return params;
  }; //--  fn  deparam
})(decodeURIComponent, /\+/g);

export default {
  isUndefined: isUndefined,
  isDefined: isDefined,
  someUndefined: someUndefined,
  someEmptyUndefinedOrNull: someEmptyUndefinedOrNull,
  allUndefined: allUndefined,
  validateEmailFormat: validateEmailFormat,
  doesNotExist: doesNotExist,
  exists: exists,
  capitalize: capitalize,
  hasUnlimitedProjects,
  prefixExporterWatermark,
  numberWithDots,
  getErrorMessageFromRequest,
  titleCase,
  updateQueryStringParameter,
  parseUrl,
  deparam
};
