# Violetta Dashboard UI
This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Requirements
* [NodeJs](https://nodejs.org/en/)
* [Yarn](https://classic.yarnpkg.com/en/docs/install/#windows-stable)


## Install & Run
```
yarn install
yarn start 
```

## Available Scripts
In the project directory, you can run:

### `yarn install`
Install all project dependencies.

### `yarn start`
Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`
Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`
Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.
The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!
See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Production build (Docker)
This project provides a Dockerfile in order to correctly build a production image, enabling its usage inside a `docker-compose` file.
* Install docker: https://www.docker.com/
* Build & tag:
    ```
    docker build -t ngoldenberg/violetta:admin-dev.1 .
    ```
* Push to docker registry:
    ```
    docker push ngoldenberg/violetta:admin-dev.1
    ```
  
Notice: docker build exposes port `80` or `8080` (depending on the target).

## Developers

### Environmental variables (build time)
* `REACT_APP_UC_APPSERVER_URL`: API URL

### Material design date picker
[This repository](https://github.com/vascofg/react-admin-date-inputs) is officially recommended by React Admin developers [here](https://github.com/marmelab/react-admin/issues/1914#issuecomment-397647592)
It hasn't been updated for a while now, but there is a PR waiting for the repository owner to merge it as stated [here](https://github.com/vascofg/react-admin-date-inputs/issues/42#issuecomment-588587272).
Meantime we can use `WiXSL`'s fork like this:
> (...) you can install from my fork adding the following to your package.json file:
```json
{ "react-admin-date-inputs": "WiXSL/react-admin-date-inputs#mui-pickers-v3" }
```
  
## Learn More
You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).
To learn React, check out the [React documentation](https://reactjs.org/).
